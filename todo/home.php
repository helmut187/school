<?php
	require_once 'connection.php';
	session_start();

	if(!isset($_SESSION['user_login']))
	{
		header("location: index.php");
	}

	$id = $_SESSION['user_login'];

	$select_stmt = $db->prepare("SELECT * FROM school WHERE username=:uid");
	$select_stmt->execute(array(":uid"=>$id));

	$row=$select_stmt->fetch(PDO::FETCH_ASSOC);

	if(isset($_SESSION['user_login']))
	{
		?>
        <form>
        	<input type="text" name="txt_titel"  placeholder="titel"><br>
        	<input type="text" name="txt_description"  placeholder="description"><br>
        	<input type="submit"  name="btn_add"  value="add entry"><br>
        </form>
    <?php
	}
	$a = 0;
	$stmt = $db->query("SELECT * FROM todo");
	
	
	
	
    while ($row = $stmt->fetch()) 
    {
        echo $row['autor']."<br>";
        echo $row['titel']."<br>";
        echo $row['description']."<br>";
        $a++;
        
        
        if($id == $row['autor'])
        {
            $topsecret = md5($row['titel']);
            
            echo "<a href='edit.php?titel=". $row['titel'] ."'>edit</a> ";
            echo "<a href='delete.php?titel=". $topsecret ."'>delete</a> ";
        }
    }
    

    
		
#----------------------------------------------------------------------------------------------------------------



if(isset($_REQUEST['btn_add']))
{
	$titel	= strip_tags($_REQUEST['txt_titel']);
	$description = strip_tags($_REQUEST['txt_description']);

	if(empty($titel))
	{
	    $errorMsg[]="enter titel";
	}
	else if(empty($description))
	{
	    $errorMsg[]="enter description";
	}
	else if(strlen($description) < 6)
	{
	    $errorMsg[] = "6 zeichen lang";
	}
	else
	{
		try
		{
			$select_stmt=$db->prepare("SELECT titel FROM todo WHERE titel =:utitel");
			$select_stmt->execute(array(':utitel'=>$titel));
			$row=$select_stmt->fetch(PDO::FETCH_ASSOC);

			if(isset($row["titel"]) ==$titel)
			{
				$errorMsg[]="titel ist vergeben";
			}

			else if(!isset($errorMsg))
			{
				$insert_stmt=$db->prepare("INSERT INTO todo(autor,titel,description) VALUES (:uautor,:utitel,:udescription)");

				if($insert_stmt->execute(array(':uautor' =>$id,	':utitel' =>$titel, ':udescription'=>$description)))
				{
					$registerMsg="ok";
					header("refresh:1; home.php");
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
}

if(isset($errorMsg))
{
	foreach($errorMsg as $error)
	{
        echo $error;
	}
}

?>