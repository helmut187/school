<?php
    session_start();
    include 'connection.php';
    
    class Model_Kunde
    {
        private $email;
    
        public function __construct($email) 
        {
            $this->email = $email;
        }
    }
    
    class View_Kunde
    {
        private $model_kunde;
    
        public function __construct(Model_Kunde $model_kunde) 
        {
            $this->model_kunde = $model_kunde;
        }
    
        public function output() 
        {
            return $this->model_kunde->email;
        }
    }
    
    
    $model_kunde = new Model_Kunde('x');
    $view_kunde = new View_Kunde($model_kunde);

    if(isset($_POST['register']))
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm = $_POST['confirm'];
     
    	if($password != $confirm)
    	{
    		$_SESSION['email'] = $email;
    		$_SESSION['password'] = $password;
    		$_SESSION['confirm'] = $confirm;
     
    		$_SESSION['error'] = 'password stimmt nicht mit dem anderen';
    	}
    	
    	else
    	{
    		$stmt = $pdo->prepare('SELECT * FROM kunde WHERE email = :email');
    		$stmt->execute(['email' => $email]);
     
    			if($stmt->rowCount() > 0)
    			{
    				$_SESSION['email'] = $email;
    				$_SESSION['password'] = $password;
    				$_SESSION['confirm'] = $confirm;
    				
    				echo "in";
    			}
    			else
    			{
    				$password = password_hash($password, PASSWORD_DEFAULT);
    				$stmt = $pdo->prepare('INSERT INTO kunde (email, pw) VALUES (:email, :password)');
     
    				try
    				{
    					$stmt->execute(['email' => $email, 'password' => $password]);
    					$_SESSION['success'] = 'kunde angelegt <a href="index.php">login</a>';
    				}
    				catch(PDOException $e)
    				{
    					$_SESSION['error'] = $e->getMessage();
    				}
    			}
    		}
    	}
    	else
    	{
    		$_SESSION['error'] = 'erst registrieren';
    	}
     
    	header('location: register_form.php');
    ?>
