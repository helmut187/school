<?php
    class Quersumme 
    {
        private $a;
    
        public function __construct($a)
        {
            $this->a = $a;
        }
    
        public function quer()
        {
            $tmp = 0;
            $array = str_split((int)$this->a);

            foreach($array as $v) 
            {
                $tmp += $v;
            }
            
            return $tmp;
        }
    }
    

    
    
    
    class Fibonacci 
    {
        private $b;
    
        public function __construct($b)
        {
            $this->b = $b;
        }
    
      function fib()
        {
            $n = $this->b;
            $f = array();
            $i;
             
            $f[0] = 0;
            $f[1] = 1;
             
            for ($i = 2; $i <= $n; $i++)
            {
                $f[$i] = $f[$i-1] + $f[$i-2];
            }
             
            return $f[$n];
        }
    }
    
    
    $calc2 = new Fibonacci(9);
    $calc = new Quersumme(1234);
    
    echo "<p>quersumme = ".$calc->quer(). "</p>";
    echo "<p>fibonacci = ".$calc2->fib(). "</p>";
    
?>


