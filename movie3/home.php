<?php
	require_once 'connection.php';
	session_start();

	if(!isset($_SESSION['user_login']))
	{
		header("location: index.php");
	}

	$id = $_SESSION['user_login'];

	$select_stmt = $db->prepare("SELECT * FROM school WHERE username=:uid");
	$select_stmt->execute(array(":uid"=>$id));

	$row=$select_stmt->fetch(PDO::FETCH_ASSOC);

	if(isset($_SESSION['user_login']))
	{
		echo "Hello mr. ".$row['username']."<br>";
		echo "<a href='logout.php'>Logout</a><br><br>";
		
		?>
		<form>
        	<input type="text" name="txt_titel"  placeholder="titel"><br>
        	<input type="text" name="txt_regisseur"  placeholder="regisseur"><br>
        	<input type="submit"  name="btn_add"  value="add movie"><br>
        </form>

    <?php
	}
		
	$stmt = $db->query("SELECT * FROM movies");
    while ($row = $stmt->fetch()) 
    {
        echo "<a href='details.php?titel=". $row['titel']."'>". $row['titel']."</a><br>";
    }
		
#----------------------------------------------------------------------------------------------------------------



if(isset($_REQUEST['btn_add']))
{
	$titel	= strip_tags($_REQUEST['txt_titel']);
	$regisseur	= strip_tags($_REQUEST['txt_regisseur']);

	if(empty($titel))
	{
	    $errorMsg[]="enter titel";
	}
	else if(empty($regisseur))
	{
	    $errorMsg[]="enter regisseur";
	}
	else if(strlen($regisseur) < 6)
	{
	    $errorMsg[] = "6 zeichen lang";
	}
	else
	{
		try
		{
			$select_stmt=$db->prepare("SELECT titel FROM movies WHERE titel =:utitel");
			$select_stmt->execute(array(':utitel'=>$titel));
			$row=$select_stmt->fetch(PDO::FETCH_ASSOC);

			if(isset($row["titel"]) ==$titel)
			{
				$errorMsg[]="titel ist vergeben";
			}

			else if(!isset($errorMsg))
			{
				$insert_stmt=$db->prepare("INSERT INTO movies(titel,regisseur) VALUES (:utitel,:uregisseur)");

				if($insert_stmt->execute(array(	':utitel' =>$titel, ':uregisseur'=>$regisseur)))
				{
					$registerMsg="ok";
					header("refresh:1; home.php");
				}
			}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
}

if(isset($errorMsg))
{
	foreach($errorMsg as $error)
	{
        echo $error;
	}
}

if(isset($registerMsg))
{
	echo $registerMsg; 
}
?>