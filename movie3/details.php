<?php
	require_once 'connection.php';
	session_start();

	if(!isset($_SESSION['user_login']))
	{
        echo "you are not logged in.<br><br>";
	}

	$id = $_SESSION['user_login'];

	$user_stmt = $db->prepare("SELECT * FROM school WHERE username=:uid");
	$user_stmt->execute(array(":uid"=>$id));

	$row = $user_stmt->fetch(PDO::FETCH_ASSOC);

	if(isset($_SESSION['user_login']))
	{
		echo "Welcome mr.".$row['username']."<br>";
		echo "<a href='logout.php'>Logout</a><br>";
		echo "<a href='home.php'>home</a><br>";
		
		$movie_stmt = $db->query("SELECT * FROM movies where titel ='".$_GET['titel']."'");
        while ($row = $movie_stmt->fetch()) 
        {
            echo $row['titel']."<br>";
        }
    
		?>
		<form>
    		<input type="text" name="txt_titel"   value="<?php echo $_GET['titel']; ?>"><br>
        	<input type="text" name="txt_autor"   value="<?php echo $id; ?>"><br>
        	<input type="text" name="txt_comment"  placeholder="comment"><br>
        	<input type="text" name="txt_rating"  placeholder="rating"><br>
        	<input type="submit"  name="btn_add"  value="add voting"><br>
        </form>

        <?php
		}
		
	    $voting_stmt = $db->query("SELECT * FROM voting where titel ='".$_GET['titel']."'");
        while ($row2 = $voting_stmt->fetch()) 
        {
            echo "<b>autor:</b>".$row2['autor']."<br />\n";
            echo "<b>titel:</b>".$row2['titel']."<br />\n";
            echo "<b>comment:</b>".$row2['review']."<br />\n";
            echo "<b>voting:</b>";
            
            if ($row2['voting'] == '1') {echo "★";}
            else if ($row2['voting'] == '2') {echo "★★";}
            else if ($row2['voting'] == '3') {echo "★★★";}
            else if ($row2['voting'] == '4') {echo "★★★★";}
            else if ($row2['voting'] == '5') {echo "★★★★★";}
            else if ($row2['voting'] >= '6') {echo "★★★★★★";}
            
            echo "<br>-----------------<br>";
        }

		
        if(isset($_REQUEST['btn_add']))
        {
        	$titel	= strip_tags($_REQUEST['txt_titel']);
        	$autor	= strip_tags($_REQUEST['txt_autor']);
        	$comment	= strip_tags($_REQUEST['txt_comment']);
        	$rating	= strip_tags($_REQUEST['txt_rating']);
		
		
	        if(empty($titel))
	        {
	            $errorMsg[]="enter titel";
	        }
        	
        	else if(empty($titel))
        	{
        	    $errorMsg[]="enter regisseur";
        	}
        	
	        else if(strlen($titel) < 2)
	        {
	            $errorMsg[] = "6 zeichen lang";
	        }
	        
	        else
	        {
    		    try
    		    {
    			    $insert_stmt=$db->prepare("INSERT INTO voting(titel,autor,review,voting) VALUES (:utitel,:uregisseur,:ureview,:uvoting)");

    			    if($insert_stmt->execute(array(	':utitel' =>$titel, ':uregisseur'=>$autor, ':ureview'=>$comment, ':uvoting'=>$rating)))
        			{
        				$registerMsg="ok";
        				header("refresh:1; details.php?titel=". $titel);	
        			}
    			
    		        }
        		catch(PDOException $e)
        		{
        			echo $e->getMessage();
        		}
	        }
        }

    	if(isset($errorMsg))
    	{
    		foreach($errorMsg as $error)
    		{
                echo $error;
    		}
    	}
    	
    	if(isset($registerMsg))
    	{
    	    echo $registerMsg;
    	}
    
?>