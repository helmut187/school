<?php
    session_start();
    require_once 'connection.php';

    if(isset($_SESSION["user_login"]))
    {
    	header("location: home.php");
    }

    if(isset($_REQUEST['btn_login']))
    {
    	$username = strip_tags($_REQUEST["txt_username"]);
    	$password = strip_tags($_REQUEST["txt_password"]);	
    		
    	if(empty($username)){						
    		$errorMsg[]="please enter username or email";
    	}
    	else if(empty($email)){
    		$errorMsg[]="please enter username or email";
    	}
    	else if(empty($password)){
    		$errorMsg[]="please enter password";
    	}
    
    	try
    	{
    		$select_stmt=$db->prepare("SELECT * FROM school WHERE username=:uname"); 
    		$select_stmt->execute(array(':uname'=>$username));
    		$row=$select_stmt->fetch(PDO::FETCH_ASSOC);
    			
    		if($select_stmt->rowCount() > 0)
    		{
    			if($username==$row["username"])
    			{
    				if(password_verify($password, $row["password"]))
    				{
    					$_SESSION["user_login"] = $row["username"];
    
    					$loginMsg = "Successfully Login...";	
    					header("refresh:1; home.php");		
    				}
    				else
    				{
    					$errorMsg[]="wrong password";
    				}
    			}
    			else
    			{
    				$errorMsg[]="wrong username or email";
    			}
    		}
    	}
    	
    	catch(PDOException $e)
    	{
    		$e->getMessage();
    	}		
    }
?>


<form method="post">
	<input type="text" name="txt_username" placeholder="username">
	<input type="password" name="txt_password"  placeholder="passowrd">
	<input type="submit" name="btn_login"  value="Login">
	<a href="register.php"="">register</a>
</form>
			
			
<?php
	$result = $db->prepare("SELECT * FROM movies ORDER BY id ASC");
	$result->execute();
		
	echo "<br><br><b>movie list</b><br>";
	for($i=0; $row = $result->fetch(); $i++)
	{
        echo "<a href='details.php?titel=". $row['titel']."'>". $row['titel']."</a><br>";
	}
?>