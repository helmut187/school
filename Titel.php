 <?php
    class Model_Titel 
    {
        public $text;
    
        public function __construct($text) 
        {
            $this->text = $text;
        }
    }
    
    class View_Titel
    {
        private $titel;
    
        public function __construct(Model_Titel $titel) 
        {
            $this->titel = $titel;
        }
    
        public function getTitel() 
        {
            return $this->titel->text;
        }
    }
    
    class Controller_Titel
    {
        private $titel;
    
        public function __construct(Model_Titel $titel) 
        {
            $this->titel = $titel;
        }
    
        public function change() 
        {
            $this->titel = $titel;
        }
    }
    ?>
